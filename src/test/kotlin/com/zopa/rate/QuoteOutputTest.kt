package com.zopa.rate

import org.amshove.kluent.shouldBeEqualTo
import org.amshove.kluent.shouldNotBeNull
import org.amshove.kluent.shouldNotBeNullOrEmpty
import org.junit.Test

class QuoteOutputTest {

    companion object {
        const val DOUBLE_VALUE_REGEX = "[0-9]+.[0-9]+";
    }

    @Test
    fun `output should be formatted properly`() {
        // given: some quote
        val quoteOutput = createTestQuoteOutput()

        // when: output is provided
        val displayedOutput = quoteOutput.toString();

        // then: it's formatted properly, having all the values
        val requestedAmount = "Requested amount: £([0-9]+)".toRegex().find(displayedOutput)
        val annualInterestRate = "Annual Interest Rate: ($DOUBLE_VALUE_REGEX)%".toRegex().find(displayedOutput)
        val monthlyRepayment = "Monthly repayment: £($DOUBLE_VALUE_REGEX)".toRegex().find(displayedOutput)
        val totalRepayment = "Total repayment: £($DOUBLE_VALUE_REGEX)".toRegex().find(displayedOutput)

        requestedAmount.shouldNotBeNull()
        annualInterestRate.shouldNotBeNull()
        monthlyRepayment.shouldNotBeNull()
        totalRepayment.shouldNotBeNull()
        requestedAmount.groupValues[1].shouldNotBeNullOrEmpty()
        annualInterestRate.groupValues[1].shouldNotBeNullOrEmpty()
        monthlyRepayment.groupValues[1].shouldNotBeNullOrEmpty()
        totalRepayment.groupValues[1].shouldNotBeNullOrEmpty()
    }

    @Test
    fun `repayment amounts should be displayed to two decimal places`() {
        // given: quote with repayment amounts with more than 2 decimal places
        val quoteOutput = createTestQuoteOutput(monthlyRepayment = 30.3456789, totalRepayment = 1108.789123324)

        // when: output is provided
        val displayedOutput = quoteOutput.toString();

        // then: monthly repayment amount should be displayed to 2 decimal places
        val monthlyRepaymen = "Monthly repayment: £($DOUBLE_VALUE_REGEX)".toRegex().find(displayedOutput)
        monthlyRepaymen.shouldNotBeNull()
        monthlyRepaymen.groupValues[1] shouldBeEqualTo "30.35"
        // and: total repayment amount should be displayed to 2 decimal places as well
        val totalRepayment = "Total repayment: £($DOUBLE_VALUE_REGEX)".toRegex().find(displayedOutput)
        totalRepayment.shouldNotBeNull()
        totalRepayment.groupValues[1] shouldBeEqualTo "1108.79"
    }

    @Test
    fun `annual interest rate should be displayed to one decimal place`() {
        // given: quote with annual interest rate with more than one decimal place
        val quoteOutput = createTestQuoteOutput(annualInterestRate = 0.0712677777)

        // when: output is provided
        val displayedOutput = quoteOutput.toString();

        // then: annual interest rate should be displayed to one decimal place
        val annualInterestRate = "Annual Interest Rate: ($DOUBLE_VALUE_REGEX)%".toRegex().find(displayedOutput)
        annualInterestRate.shouldNotBeNull()
        annualInterestRate.groupValues[1] shouldBeEqualTo "7.1"
    }
}