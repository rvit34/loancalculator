package com.zopa.rate

import org.amshove.kluent.`should be equal to`
import org.amshove.kluent.`should be positive`
import org.amshove.kluent.`should be`
import org.junit.Test

class LenderMarketTest {

    @Test
    fun `annual interest rate should be the average of lenders offers`() {
        // given: market has 3 offers
        val market = InMemoryLenderMarket()
        market.addAll(arrayListOf(
            Lender(name = "Ann", rate = 0.015, available = 300),
            Lender(name = "John", rate = 0.02, available = 200),
            Lender(name = "Bob",  rate = 0.03, available = 500)
        ))

        // when: requested amount is exactly fulfilled by market
        val rate = market.annualInterestRate(1000)

        // then: rate should be the average of lenders offers according to amount of each offer
        val expectedRate = (0.015 * 300 + 0.02 * 200 + 0.03 * 500) / 1000;
        rate `should be equal to` expectedRate
    }

    @Test
    fun `annual interest rate should be as lower as possible`() {
        // given: market has multiple offers
        val market = InMemoryLenderMarket()
        market.addAll(arrayListOf(
            Lender(name = "Bob", rate = 0.075, available = 640),
            Lender(name = "Jane", rate = 0.069, available = 480),
            Lender(name = "Fred",  rate = 0.071, available = 520),
            Lender(name = "Mary",  rate = 0.104, available = 170),
            Lender(name = "John",  rate = 0.081, available = 320),
            Lender(name = "Dave",  rate = 0.074, available = 140),
            Lender(name = "Angela",  rate = 0.071, available = 60),
        ))

        // when: requested amount is fulfilled by a few offers in the market
        val rate = market.annualInterestRate(1000)

        // then: rate should be the average of lenders offers according to lowest rates and amount of each offer
        val expectedRate = (0.069 * 480 + 0.071 * 520) / 1000;
        rate.roundTo(3) `should be equal to` expectedRate.roundTo(3)
    }

    @Test
    fun `market might not have enought liquidity to fulfill request`() {
        // given: market with limited liquidity
        val market = InMemoryLenderMarket()
        market.addAll(arrayListOf(
            Lender(name = "Bob", rate = 0.075, available = 50),
            Lender(name = "Dave",  rate = 0.074, available = 70),
            Lender(name = "Angela",  rate = 0.071, available = 20),
        ))

        // when: requested amount exceeds market liquidity
        val rate = market.annualInterestRate(200)

        // then: rate is negative
        rate `should be equal to` -1.0
    }

    @Test
    fun `lenders with incorrect data should not be admitted into the market`() {
        // given: market with two incorrect offers
        val market = InMemoryLenderMarket()
        market.addAll(arrayListOf(
            Lender(name = "Bob", rate = 0.075, available = 50),
            Lender(name = "Tom", rate = 0.005, available = -500),
            Lender(name = "Jerry", rate = 0.0, available = 5000),
        ))

        // then: market should contain only Bob's offer
        market.offersCount() `should be` 1
    }

    @Test
    fun `twin lenders test`() {
        val market = InMemoryLenderMarket()
        market.addAll(arrayListOf(
            Lender(name = "Bob", rate = 0.075, available = 50),
            Lender(name = "Tom", rate = 0.08, available = 500),
            Lender(name = "Tom", rate = 0.08, available = 200),
            Lender(name = "Tom", rate = 0.005, available = 500),
            Lender(name = "Tom", rate = 0.005, available = 500),
        ))

        // when: requested amount does not exceed market liquidity
        val rate = market.annualInterestRate(1750)

        // then: rate is positive cause market should take into account twin lenders
        rate.`should be positive`()
    }
}