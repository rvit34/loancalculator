package com.zopa.rate

import org.amshove.kluent.shouldBeEmpty
import org.amshove.kluent.shouldNotBeEmpty
import org.junit.Test

class RequestValidatorTest {

    private val validator = QuoteRequestValidatorImpl()

    @Test
    fun `amount validation test`() {
        // negative cases
        validator.validateAmount("fhf%323").shouldNotBeEmpty()
        validator.validateAmount("10.545").shouldNotBeEmpty()
        validator.validateAmount("999").shouldNotBeEmpty()
        validator.validateAmount("15001").shouldNotBeEmpty()
        validator.validateAmount("1104").shouldNotBeEmpty()

        // positive cases
        validator.validateAmount("1000").shouldBeEmpty()
        validator.validateAmount("1100").shouldBeEmpty()
        validator.validateAmount("14900").shouldBeEmpty()
        validator.validateAmount("15000").shouldBeEmpty()
    }

    @Test
    fun `file validation test`() {
        // negative cases
        validator.validateFile("").shouldNotBeEmpty()
        validator.validateFile(".txt").shouldNotBeEmpty()

        // positive cases
        validator.validateFile(testMarketDataFile().absolutePath).shouldBeEmpty()
    }
}