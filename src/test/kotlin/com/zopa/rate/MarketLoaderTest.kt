package com.zopa.rate

import org.amshove.kluent.`should be equal to`
import org.amshove.kluent.`should be`
import org.junit.Assert.*
import org.junit.Test

class MarketLoaderTest {

    @Test
    fun `loader should fetch market data from csv file correctly`() {
        // given: market data presented in file
        val file = testMarketDataFile()

        // then: file should exist and can be read
        assertTrue("File ${file.absolutePath} does not exist", file.exists())
        assertTrue("File ${file.absolutePath} cannot be read", file.canRead())

        // when: loader fetches data from file
        val loader = CSVMarketLoader(file, InMemoryLenderMarket())
        val market = loader.loadLenderMarket()

        // then: market data should correspond to data in file
        market.offersCount() `should be` 7
        market.availableAmount() `should be equal to` 2330
        market.annualInterestRate().toPercentage().roundTo(2) `should be equal to` 17.63
    }
}