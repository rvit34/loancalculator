package com.zopa.rate

import java.io.File

fun createTestQuoteOutput(
     requestedAmount:Int = 1000,
     annualInterestRate: Double = 7.0,
     monthlyRepayment: Double = 30.78,
     totalRepayment: Double = 1108.10
 ) = QuoteOutput(requestedAmount, annualInterestRate, monthlyRepayment, totalRepayment)

fun testMarketDataFile() = File(MarketLoaderTest::class.java.getResource("./../../../market_data_example.csv")!!.file)