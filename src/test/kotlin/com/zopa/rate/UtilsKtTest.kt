package com.zopa.rate

import org.amshove.kluent.`should be equal to`
import org.junit.Test

import org.junit.Assert.*

class UtilsKtTest {

    @Test
    fun calcMonthlyRepaymentTest() {
        // given: 7% annual interest rate
        val annualInterestRate = 0.07
        val monthlyRepayment = calcMonthlyRepayment(1000, annualInterestRate / 12.0)
        monthlyRepayment.roundTo(2) `should be equal to` 30.88
    }
}