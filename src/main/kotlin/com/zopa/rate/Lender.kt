package com.zopa.rate

data class Lender (
    val name: String,
    val rate: Double,
    var available: Int
) : Comparable<Lender> {

    fun increaseAvailableAmount(amount: Int) {
        available += amount;
    }

    override fun compareTo(other: Lender): Int {
        if (this == other) return 0
        val res = rate.compareTo(other.rate)
        return if (res == 0) {
            available.compareTo(other.available) * -1
        } else {
            res;
        }
    }
}