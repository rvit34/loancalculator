package com.zopa.rate

data class QuoteOutput(
    val requestedAmount: Int,
    val annualInterestRate: Double,
    val monthlyRepayment: Double,
    val totalRepayment: Double
) {

    companion object {
        const val CURRENCY_SIGN = '£';
    }

    override fun toString(): String {
        return """
            Requested amount: $CURRENCY_SIGN$requestedAmount
            Annual Interest Rate: ${annualInterestRate.toPercentage().roundTo(1)}%
            Monthly repayment: $CURRENCY_SIGN${monthlyRepayment.roundTo(2)}
            Total repayment: $CURRENCY_SIGN${totalRepayment.roundTo(2)}
        """.trimIndent()
    }
}
