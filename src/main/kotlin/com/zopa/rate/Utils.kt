package com.zopa.rate

import java.util.Locale

fun Double.roundTo(decimalPlaces: Int) = "%.${decimalPlaces}f".format(this, Locale.ENGLISH).toDouble()

fun Double.toPercentage() = this * 100

/**
 * calculates monthly repayment based on the formula https://en.wikipedia.org/wiki/Amortization_calculator#The_formula
 */
fun calcMonthlyRepayment(requestedAmount: Int, interestRate: Double, periods: Int = 36): Double {
    return requestedAmount * (interestRate + (interestRate / ( Math.pow(1 + interestRate, periods.toDouble()) - 1)))
}

fun totalRepayment(monthlyRepayment: Double, periods: Int = 36) = monthlyRepayment * periods

fun printError(err: String) = System.err.println(err)


