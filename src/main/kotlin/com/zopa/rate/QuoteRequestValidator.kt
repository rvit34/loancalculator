package com.zopa.rate

import java.io.File

interface QuoteRequestValidator {
    fun validateFile(fileName: String): String
    fun validateAmount(amount: String): String
}

fun QuoteRequestValidator.validateArgs(args: Array<String>):String {
    if (args.size == 1 && args[0] == "-h") {
        return """
        USAGE: two arguments are required:
            - path to csv file with market data;
            - loan amount. Limits: [1000£-15000£]. Step: 100£.
        Example: java -jar zopa-rate.jar market_data.csv 1000 
    """.trimIndent()
    }
    if (args.isEmpty() || args.size < 2) {
        return "You have not specified required arguments. Run program with -h argument to see usage"
    }
    val fileValidationError = validateFile(args[0])
    if (fileValidationError != "") {
        return fileValidationError
    }
    return validateAmount(args[1])
}

class QuoteRequestValidatorImpl: QuoteRequestValidator {

    override fun validateFile(fileName: String): String {
        val file = File(fileName)
        if (!file.exists() || !file.canRead()) {
            return "file $fileName does not exist or cannot be read"
        }
        if (file.isFile.not()) {
            return "file $fileName is not a file"
        }
        return ""
    }

    override fun validateAmount(amount: String): String {
        val requestedAmount:Int
        try {
            requestedAmount = amount.toInt()
        } catch (ex: NumberFormatException) {
            return "amount is not an integer value"
        }

        if (requestedAmount < 1000 || requestedAmount > 15000) {
            return "amount is out of range [1000 - 15000]"
        }

        if (requestedAmount % 100 != 0) {
            return "amount step does not correspond to 100"
        }
        return ""
    }
}