package com.zopa.rate

import java.io.File
import java.lang.Exception
import kotlin.system.exitProcess

fun main(args: Array<String>) {
    val validator = QuoteRequestValidatorImpl()
    val err = validator.validateArgs(args)
    if (err.isEmpty().not()) {
        printError(err)
        exitProcess(1)
    }
    val file = File(args[0])
    val requestedAmount = args[1].toInt()

    val marketLoader = CSVMarketLoader(file, InMemoryLenderMarket())
    val market: LenderMarket
    try {
        market = marketLoader.loadLenderMarket()
    } catch (ex: Exception) {
        printError("could not load market data from file ${file.absolutePath}. Probably it's corrupted or not match to market data format")
        exitProcess(1)
    }
    val annualInterestRate = market.annualInterestRate(requestedAmount)
    if (annualInterestRate == -1.0) {
        printError("market does not have enough offers to fulfill your request")
        exitProcess(1)
    }
    val monthlyRepayment = calcMonthlyRepayment(requestedAmount, annualInterestRate / 12) // for simplicity purposes just divide by 12
    val totalRepayment = totalRepayment(monthlyRepayment)
    val output = QuoteOutput(requestedAmount, annualInterestRate, monthlyRepayment, totalRepayment)
    println(output)
}