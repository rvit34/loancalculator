package com.zopa.rate

import java.math.BigInteger
import java.math.RoundingMode
import java.util.TreeSet

interface LenderMarket {

    /**
     * adds specified lender to the market.
     * If market already contains the same lender (name, rate and available amount are equals) then
     * it treats it as one lender in sum of amounts.
     * lenders with negative rates or amounts are not accepted
     */
    fun addLender(lender: Lender)

    /**
     * calculates annual interest rate by accumulating the all rates of lenders in the market
     * @return annual interest rate or -1.0 if the market is empty
     */
    fun annualInterestRate(): Double

    /**
     * calculates annual interest rate by accumulating the lowest rates of lenders in the market
     * @return annual interest rate or -1.0 if the market does not have enough offers
     */
    fun annualInterestRate(requestedAmount: Int): Double

    /**
     * returns the count of all offers in the market
     */
    fun offersCount(): Int

    /**
     * returns sum of all offers in the market
     */
    fun availableAmount(): Int
}

class InMemoryLenderMarket : LenderMarket {

    private val market = TreeSet<Lender>()

    companion object {
        private const val RATE_MULTIPLIER = 1000.0 // depends on decimal points in lender's rate
    }

    override fun offersCount() = market.size

    override fun availableAmount() = market.fold(0) { acc, lender -> acc + lender.available }

    override fun annualInterestRate(): Double {
        return if (market.isEmpty()) {
            -1.0
        } else {
            val rate = market.fold(0.0) { acc, lender -> acc + (lender.rate * RATE_MULTIPLIER).toInt() * lender.available }
            return rate / (RATE_MULTIPLIER * RATE_MULTIPLIER)
        }
    }

    fun addAll(lenders: List<Lender>) = lenders.forEach { addLender(it) }

    override fun addLender(lender: Lender) {
        if (lender.rate <= 0 || lender.available <= 0) {
            return
        }
        if (!market.add(lender)) {
            val existedLender = market.elementAtOrNull(market.indexOf(lender))
            existedLender?.increaseAvailableAmount(lender.available)
        }
    }

    override fun annualInterestRate(requestedAmount: Int): Double {
        var amount = requestedAmount
        var rate = 0
        var diff = 0
        for (lender in market) {
            diff = amount - lender.available
            if (diff >= 0) {
                // lender's amount can be completely included to interest rate
                amount = diff
                rate += (lender.rate * RATE_MULTIPLIER).toInt() * lender.available
            } else {
                // lender's amount will be partly included to interest rate
                rate += (lender.rate * RATE_MULTIPLIER).toInt() * amount
                amount = 0
            }
            if (amount == 0) {
                return rate / (RATE_MULTIPLIER * RATE_MULTIPLIER);
            }
        }
        return -1.0
    }
}