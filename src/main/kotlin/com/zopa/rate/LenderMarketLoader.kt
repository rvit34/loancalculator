package com.zopa.rate

import com.github.doyaaaaaken.kotlincsv.dsl.csvReader
import java.io.File

interface LenderMarketLoader {
    fun loadLenderMarket(): LenderMarket
}

class CSVMarketLoader(
    val csvFile: File,
    val market: LenderMarket
) : LenderMarketLoader {

    override fun loadLenderMarket(): LenderMarket {
        csvReader().readAll(csvFile).forEachIndexed { index, record ->
            if (index != 0) {
                market.addLender(
                    Lender(
                        name = record[0],
                        rate = record[1].toDouble(),
                        available = record[2].toInt())
                )
            }
        }
        return market
    }
}