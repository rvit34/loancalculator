
**Description**

Here is my implementation of application which might be used to find a quote from 36-month loans.</br>
Details of task can be found in [ZopaTechnicalTest.pdf](/ZopaTechnicalTest.pdf)

**Prerequisites**

You need JDK8 or higher installed

**Build**

Build the project from project root directory :

    ./mvnw clean package

**Launch**

If you want to run a user shell (cli) just run next command with input arguments

    ./zopa-rate.sh ./src/test/resources/market_data_example.csv 1000
or 
    ```java -jar ./target/zopa-rate.jar ./src/test/resources/market_data_example.csv 1000```